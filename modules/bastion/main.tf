resource "aws_instance" "bastion" {
    ami = "${var.image_id}"
    vpc_security_group_ids = [ "${var.public_sg_id}" ]
    instance_type = "${var.instance_type}"
    key_name      = "${var.key_name}"
    associate_public_ip_address = true
    tags {
        Name = "${var.environment}-bastion"
    }
    subnet_id = "${element(var.public_subnets_id, count.index)}"

    depends_on = ["aws_instance.bastion"]

    user_data = <<HEREDOC
    #!/bin/bash
    
    yum -y install git
    git clone https://ThisIsRahmat@bitbucket.org/JangleFett/petclinic.git
    yum -y install mysql
    mysql -u ${var.database_username} -h ${var.database_dns} -p${var.database_password} <petclinic/src/main/resources/db/mysql/schema.sql
    mysql -u ${var.database_username} -h ${var.database_dns} -p${var.database_password} <petclinic/src/main/resources/db/mysql/data.sql
HEREDOC
}
