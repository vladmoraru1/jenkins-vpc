resource "aws_alb" "main" {
  name            = "${var.environment}-ecs-alb"
  subnets         = [ "${var.public_subnets_id}" ]
  security_groups = [ "${aws_security_group.lb.id}" ]
}

resource "aws_alb_target_group" "web" {
  name        = "a4-ecs-alb-target-group"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = "${var.vpc_id}"
  target_type = "ip"
  slow_start  = 30

  health_check {
    timeout   = 60
    interval  = 120
  }
}

resource "aws_alb_listener" "openjobs" {
  load_balancer_arn = "${aws_alb.main.id}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.web.id}"
    type             = "forward"
  }
}

resource "aws_route53_record" "alb" {
  zone_id = "${var.public_zone_id}"
  name    = "a4-petclinic"
  type    = "CNAME"

  alias {
    name                   = "${aws_alb.main.dns_name}"
    zone_id                = "${aws_alb.main.zone_id}"
    evaluate_target_health = true
  }
}
