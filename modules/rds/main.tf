resource "aws_route53_zone" "private" {
  vpc_id = "${var.vpc_id}"
  name   = "${var.environment}-private.com"
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "${var.environment}-rds-subnet-group"
  description = "a4 RDS subnet group"
  subnet_ids  = [ "${var.private_subnets_id}" ]
  tags {
    Environment = "${var.environment}"
  }
}

resource "aws_security_group" "db_access_sg" {
  vpc_id      = "${var.vpc_id}"
  name        = "${var.environment}-db-access-sg"
  description = "Allow access to RDS"

  tags {
    Name        = "${var.environment}-db-access-sg"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group" "rds_sg" {
  name = "${var.environment}-rds-sg"
  description = "${var.environment} DB Security Group"
  vpc_id = "${var.vpc_id}"
  tags {
    Name = "${var.environment}-rds-sg"
    Environment =  "${var.environment}"
  }

  // allows traffic from the SG itself
  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      self = true
  }

  //allow traffic for TCP 3306
  ingress {
      from_port = 3306
      to_port   = 3306
      protocol  = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  // outbound internet access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "rds" {
  identifier          = "${var.environment}-database"
  allocated_storage   = "${var.allocated_storage}"
  engine              = "mysql"
  engine_version      = "5.6.35"
  instance_class      = "db.t2.micro"
  name                = "${var.database_name}"
  username            = "${var.database_username}"
  password            = "${var.database_password}"
  multi_az            = "true"
  publicly_accessible = "true"
  vpc_security_group_ids  = ["${aws_security_group.rds_sg.id}"]
  db_subnet_group_name    = "${aws_db_subnet_group.rds_subnet_group.id}"
  skip_final_snapshot     = "true"
  # snapshot_identifier    = "rds-${var.environment}-snapshot"
  tags {
    Environment = "${var.environment}"
  }
}

resource "aws_route53_record" "rds" {
      zone_id = "${aws_route53_zone.private.zone_id}"
      name = "a4-rds"
      type = "CNAME"
      ttl = "30"
      records = ["${element(split(":", aws_db_instance.rds.endpoint), 0)}"]
}
