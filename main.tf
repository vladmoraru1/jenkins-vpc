module "vpc" {
  source               = "./modules/vpc"
  environment          = "${var.environment}"
  vpc_cidr             = "${var.vpc_cidr}"
  public_subnets_cidr  = "${var.public_subnets_cidr}"
  private_subnets_cidr = "${var.private_subnets_cidr}"
  region               = "${var.region}"
  availability_zones   = "${var.availability_zones}"
  key_name             = "${var.key_name}"
}

module "rds" {
  source               = "./modules/rds"
  environment          = "${var.environment}"
  private_subnets_id   = "${module.vpc.private_subnets_id}"
  vpc_id               = "${module.vpc.vpc_id}"
  allocated_storage    = "${var.allocated_storage}"
  database_username    = "${var.database_username}"
  database_password    = "${var.database_password}"
  database_name        = "${var.database_name}"
}

module "bastion" {
  source               = "./modules/bastion"
  environment          = "${var.environment}"
  image_id             = "${var.image_id}"
  instance_type        = "${var.instance_type}"
  key_name             = "${var.key_name}"
  vpc_id               = "${module.vpc.vpc_id}"
  public_subnets_id    = "${module.vpc.public_subnets_id}"
  public_sg_id         = "${module.vpc.public_sg_id}"
  database_username    = "${var.database_username}"
  database_password    = "${var.database_password}"
  database_dns         = "${var.database_dns}"
}

module "ecs" {
  source               = "./modules/ecs"
  environment          = "${var.environment}"
  vpc_id               = "${module.vpc.vpc_id}"
  public_subnets_id    = "${module.vpc.public_subnets_id}"
  private_subnets_id   = "${module.vpc.private_subnets_id}"
  repository_name      = "${var.repository_name}"
  secret_key_base      = "${var.secret_key_base}"
  database_username    = "${var.database_username}"
  database_password    = "${var.database_password}"
  database_name        = "${var.database_name}"
  database_endpoint    = "${module.rds.database_endpoint}"
  execution_role_arn   = "${var.execution_role_arn}"
  public_zone_id       = "${var.public_zone_id}"
}
