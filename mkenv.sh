#!/bin/#!/usr/bin/env bash

# source mkenv

TF_VAR_access_key=$(grep -i 'aws_access_key' ~/.aws/credentials | awk '{print $NF}')

TF_VAR_secret_key=$(grep -i 'aws_secret_access_key' ~/.aws/credentials | awk '{print $NF}')

export TF_VAR_access_key TF_VAR_secret_key
